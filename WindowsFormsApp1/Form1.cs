﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO; //include-amo jer moramo koristiti datoteku pohranjenu na disku

namespace WindowsFormsApp1
{
    public partial class frm_HangMan : Form
    {
        private int attempts = 0;
        private string[] words;
        private string current = "";
        private string copyCurrent = "";//kopija trenutne rijeci, zbog broja crtica
        public frm_HangMan()
        {
            InitializeComponent();
        }
        private void Wordloading()
        {
            //char separator = ',';
            char[] separator = { ',' };
            string[] readText = File.ReadAllLines("rijeci_hangman.csv");
            //citanje redova iz datoteke rijeci koju smo sami napravili
            //csv datoteka = comma separated values, zarezom odvojene vrijednosti
            //tu dat smo stavili u projekt pod bin -> Debug
            words = new string[readText.Length];//kreiranje polja stringova velicine jednake 
                                                //broju rijeci koliko je u file-u
            int index = 0;
            foreach (string s in readText)
            {
                string[] line = s.Split(separator);//uzima zarez kao znak koji odvaja rijeci
                words[index++] = line[1];//uzima prvu rijec nakon zareza, trazene rijeci za igru
            }    
        }
        private void randomWord()
        {
            attempts = 0;
            int randIdx = (new Random()).Next(words.Length); //random generiranje, metoda next izbacuje 
            //nenegativni nasumicni integer, Random je klasa vec odredena 
            current = words[randIdx]; //current-u se pridruzuje nasumicna rijec iz popisa
            copyCurrent = "";
            for(int i = 0; i < current.Length; i++)
            {
                copyCurrent += "_";//copyCurrent je samo broj crtica rijeci
            }
            showCopy();//pozivanje ispisa crtica na ekranu
        }
            

        private void showCopy()
        {
            lbl_showWord.Text = "";
            for(int i = 0; i < copyCurrent.Length; i++)
            {
                lbl_showWord.Text += copyCurrent.Substring(i, 1);//odredeni dio stringa se ispisuje
                //na label-u, i je pocetni index, a 1 je duljina 
                lbl_showWord.Text += " "; //da se vidi razmak izmedu slova
            }
        }

        

        private void letterGuess_Click(object sender, EventArgs e)
        {
            Button button = sender as Button;
            button.Enabled = false;//onemogucuje ponovni pokusaj istog slova
            if (current.Contains(button.Text))//ako je slovo koje pogadamo u tom stringu, crticu 
                //mijenja sa tim slovom
            {
                char[] temp = copyCurrent.ToCharArray();
                char[] find = current.ToCharArray();
                char guessChar = button.Text.ElementAt(0);
                for(int i = 0; i < find.Length; i++)
                {
                    if(find[i] == guessChar)//ako je pritisnut gumb jednak nekom slovu u rijeci
                    {
                        temp[i] = guessChar;//crticu zamijenimo pritisnutim gumbom
                    }
                }
                copyCurrent = new string(temp);
                showCopy();
            }
            else
            {
                attempts++;
            }
            if (attempts < 7)
            {
                lbl_result.Text = "preostalo je " + (7 - attempts).ToString() + " pokusaja.";
                //ako se pogrijesi slovo, broj pokusaja raste sve dok ne dode do 7
                //ovdje ispisujemo koliko je preostalo
            }
            else
            {
                lbl_result.Text = "IZGUBILI STE!";
            }
            if (copyCurrent == current)
            {
                lbl_result.Text = "POBJEDA!";
            }
        }

        private void frm_HangMan_Load(object sender, EventArgs e)
        {
            Wordloading();
            randomWord();

        }

        private void quit_Click(object sender, EventArgs e)
        {
            Application.Exit();
           
        }
    }
}
