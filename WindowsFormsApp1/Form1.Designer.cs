﻿namespace WindowsFormsApp1
{
    partial class frm_HangMan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_1 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.button19 = new System.Windows.Forms.Button();
            this.button20 = new System.Windows.Forms.Button();
            this.button21 = new System.Windows.Forms.Button();
            this.button22 = new System.Windows.Forms.Button();
            this.button23 = new System.Windows.Forms.Button();
            this.button24 = new System.Windows.Forms.Button();
            this.button25 = new System.Windows.Forms.Button();
            this.lbl_showWord = new System.Windows.Forms.Label();
            this.button27 = new System.Windows.Forms.Button();
            this.lbl_result = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button_1
            // 
            this.button_1.Location = new System.Drawing.Point(12, 12);
            this.button_1.Name = "button_1";
            this.button_1.Size = new System.Drawing.Size(40, 40);
            this.button_1.TabIndex = 0;
            this.button_1.Text = "A";
            this.button_1.UseVisualStyleBackColor = true;
            this.button_1.Click += new System.EventHandler(this.letterGuess_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(58, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(40, 40);
            this.button1.TabIndex = 1;
            this.button1.Text = "B";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.letterGuess_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(196, 58);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(40, 40);
            this.button2.TabIndex = 2;
            this.button2.Text = "R";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.letterGuess_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(518, 12);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(40, 40);
            this.button3.TabIndex = 3;
            this.button3.Text = "L";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.letterGuess_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(150, 58);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(40, 40);
            this.button4.TabIndex = 4;
            this.button4.Text = "P";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.letterGuess_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(104, 58);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(40, 40);
            this.button5.TabIndex = 5;
            this.button5.Text = "O";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.letterGuess_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(58, 58);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(40, 40);
            this.button6.TabIndex = 6;
            this.button6.Text = "N";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.letterGuess_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(12, 58);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(40, 40);
            this.button7.TabIndex = 7;
            this.button7.Text = "M";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.letterGuess_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(472, 12);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(40, 40);
            this.button8.TabIndex = 8;
            this.button8.Text = "K";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.letterGuess_Click);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(426, 12);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(40, 40);
            this.button9.TabIndex = 9;
            this.button9.Text = "J";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.letterGuess_Click);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(380, 12);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(40, 40);
            this.button10.TabIndex = 10;
            this.button10.Text = "I";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.letterGuess_Click);
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(334, 12);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(40, 40);
            this.button11.TabIndex = 11;
            this.button11.Text = "H";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.letterGuess_Click);
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(288, 12);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(40, 40);
            this.button12.TabIndex = 12;
            this.button12.Text = "G";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.letterGuess_Click);
            // 
            // button13
            // 
            this.button13.Location = new System.Drawing.Point(242, 12);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(40, 40);
            this.button13.TabIndex = 13;
            this.button13.Text = "F";
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.letterGuess_Click);
            // 
            // button14
            // 
            this.button14.Location = new System.Drawing.Point(196, 12);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(40, 40);
            this.button14.TabIndex = 14;
            this.button14.Text = "E";
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Click += new System.EventHandler(this.letterGuess_Click);
            // 
            // button15
            // 
            this.button15.Location = new System.Drawing.Point(150, 12);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(40, 40);
            this.button15.TabIndex = 15;
            this.button15.Text = "D";
            this.button15.UseVisualStyleBackColor = true;
            this.button15.Click += new System.EventHandler(this.letterGuess_Click);
            // 
            // button16
            // 
            this.button16.Location = new System.Drawing.Point(104, 12);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(40, 40);
            this.button16.TabIndex = 16;
            this.button16.Text = "C";
            this.button16.UseVisualStyleBackColor = true;
            this.button16.Click += new System.EventHandler(this.letterGuess_Click);
            // 
            // button17
            // 
            this.button17.Location = new System.Drawing.Point(472, 58);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(40, 40);
            this.button17.TabIndex = 17;
            this.button17.Text = "Q";
            this.button17.UseVisualStyleBackColor = true;
            this.button17.Click += new System.EventHandler(this.letterGuess_Click);
            // 
            // button18
            // 
            this.button18.Location = new System.Drawing.Point(426, 58);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(40, 40);
            this.button18.TabIndex = 18;
            this.button18.Text = "Z";
            this.button18.UseVisualStyleBackColor = true;
            this.button18.Click += new System.EventHandler(this.letterGuess_Click);
            // 
            // button19
            // 
            this.button19.Location = new System.Drawing.Point(380, 58);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(40, 40);
            this.button19.TabIndex = 19;
            this.button19.Text = "V";
            this.button19.UseVisualStyleBackColor = true;
            this.button19.Click += new System.EventHandler(this.letterGuess_Click);
            // 
            // button20
            // 
            this.button20.Location = new System.Drawing.Point(334, 58);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(40, 40);
            this.button20.TabIndex = 20;
            this.button20.Text = "U";
            this.button20.UseVisualStyleBackColor = true;
            this.button20.Click += new System.EventHandler(this.letterGuess_Click);
            // 
            // button21
            // 
            this.button21.Location = new System.Drawing.Point(288, 58);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(40, 40);
            this.button21.TabIndex = 21;
            this.button21.Text = "T";
            this.button21.UseVisualStyleBackColor = true;
            this.button21.Click += new System.EventHandler(this.letterGuess_Click);
            // 
            // button22
            // 
            this.button22.Location = new System.Drawing.Point(242, 58);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(40, 40);
            this.button22.TabIndex = 22;
            this.button22.Text = "S";
            this.button22.UseVisualStyleBackColor = true;
            this.button22.Click += new System.EventHandler(this.letterGuess_Click);
            // 
            // button23
            // 
            this.button23.Location = new System.Drawing.Point(58, 104);
            this.button23.Name = "button23";
            this.button23.Size = new System.Drawing.Size(40, 40);
            this.button23.TabIndex = 23;
            this.button23.Text = "X";
            this.button23.UseVisualStyleBackColor = true;
            this.button23.Click += new System.EventHandler(this.letterGuess_Click);
            // 
            // button24
            // 
            this.button24.Location = new System.Drawing.Point(12, 104);
            this.button24.Name = "button24";
            this.button24.Size = new System.Drawing.Size(40, 40);
            this.button24.TabIndex = 24;
            this.button24.Text = "Y";
            this.button24.UseVisualStyleBackColor = true;
            this.button24.Click += new System.EventHandler(this.letterGuess_Click);
            // 
            // button25
            // 
            this.button25.Location = new System.Drawing.Point(518, 58);
            this.button25.Name = "button25";
            this.button25.Size = new System.Drawing.Size(40, 40);
            this.button25.TabIndex = 25;
            this.button25.Text = "W";
            this.button25.UseVisualStyleBackColor = true;
            this.button25.Click += new System.EventHandler(this.letterGuess_Click);
            // 
            // lbl_showWord
            // 
            this.lbl_showWord.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lbl_showWord.Location = new System.Drawing.Point(12, 170);
            this.lbl_showWord.Name = "lbl_showWord";
            this.lbl_showWord.Size = new System.Drawing.Size(545, 41);
            this.lbl_showWord.TabIndex = 26;
            this.lbl_showWord.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button27
            // 
            this.button27.Location = new System.Drawing.Point(17, 353);
            this.button27.Name = "button27";
            this.button27.Size = new System.Drawing.Size(108, 42);
            this.button27.TabIndex = 28;
            this.button27.Text = "EXIT";
            this.button27.UseVisualStyleBackColor = true;
            this.button27.Click += new System.EventHandler(this.quit_Click);
            // 
            // lbl_result
            // 
            this.lbl_result.Font = new System.Drawing.Font("Microsoft Sans Serif", 22.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lbl_result.ForeColor = System.Drawing.Color.DarkRed;
            this.lbl_result.Location = new System.Drawing.Point(288, 280);
            this.lbl_result.Name = "lbl_result";
            this.lbl_result.Size = new System.Drawing.Size(442, 115);
            this.lbl_result.TabIndex = 29;
            this.lbl_result.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // frm_HangMan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.lbl_result);
            this.Controls.Add(this.button27);
            this.Controls.Add(this.lbl_showWord);
            this.Controls.Add(this.button25);
            this.Controls.Add(this.button24);
            this.Controls.Add(this.button23);
            this.Controls.Add(this.button22);
            this.Controls.Add(this.button21);
            this.Controls.Add(this.button20);
            this.Controls.Add(this.button19);
            this.Controls.Add(this.button18);
            this.Controls.Add(this.button17);
            this.Controls.Add(this.button16);
            this.Controls.Add(this.button15);
            this.Controls.Add(this.button14);
            this.Controls.Add(this.button13);
            this.Controls.Add(this.button12);
            this.Controls.Add(this.button11);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.button_1);
            this.Name = "frm_HangMan";
            this.Text = "Hangman";
            this.Load += new System.EventHandler(this.frm_HangMan_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button_1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.Button button21;
        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.Button button23;
        private System.Windows.Forms.Button button24;
        private System.Windows.Forms.Button button25;
        private System.Windows.Forms.Label lbl_showWord;
        private System.Windows.Forms.Button button27;
        private System.Windows.Forms.Label lbl_result;
    }
}

